const { src, dest, series, parallel, watch } = require("gulp");

const plumber = require("gulp-plumber");
const sass = require("gulp-sass");
sass.compiler = require("node-sass");
const browserSync = require("browser-sync").create();

async function css() {
  src("src/scss/style.scss")
    .pipe(plumber())
    .pipe(sass())
    .pipe(dest("dist/"))
    .pipe(browserSync.stream());
}

async function html() {
  src("src/*.html").pipe(dest("dist/"));
}

async function images() {
  src("src/img/*").pipe(dest("dist/img"));
}

async function fonts() {
  src("src/fonts/*").pipe(dest("dist/fonts"));
}

async function js() {
  src("src/js/*").pipe(dest("dist/js"));
}

async function browserSyncInit() {
  browserSync.init({
    server: {
      baseDir: "./dist/"
    }
  });
}

async function browserSyncReload() {
  browserSync.reload();
}

async function watchAll() {
  watch("src/scss/**/*.scss", css);
  watch("src/*.html", series(html, browserSyncReload));
  watch("src/img/*", series(images, browserSyncReload));
  watch("src/fonts/*", series(fonts, browserSyncReload));
  watch("src/js/*", series(js, browserSyncReload));
}

exports.default = series(
  parallel(css, html, images, fonts, js),
  watchAll,
  browserSyncInit
);
