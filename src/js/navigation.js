const navigation = document.querySelector(".main-navigation");
const navigationTrigger = document.querySelector(".main-navigation__trigger");

navigationTrigger.addEventListener("click", () => {
  navigation.classList.toggle("main-navigation--closed");
});
