const cartAdd = document.querySelector(".cart-add");
const modalOverlay = document.querySelector(".modal-overlay");
modalOverlay.addEventListener("click", evt => {
  if (evt.target === modalOverlay) {
    cartAdd.style.display = "none";
    modalOverlay.style.display = "none";
  }
});

const catalogItems = document.querySelectorAll(".catalog__item");
Array.prototype.map.call(catalogItems, item => {
  item.addEventListener("click", evt => {
    evt.preventDefault();

    modalOverlay.style.display = "block";
    cartAdd.style.display = "block";
  });
});

const video = document.querySelector(".manufacturing__video");
const play = document.querySelector(".manufacturing__play");

play.addEventListener("click", () => {
  video.controls = true;
  video.play();
  play.style.display = "none";
});
