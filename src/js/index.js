const cartAdd = document.querySelector(".cart-add");
const modalOverlay = document.querySelector(".modal-overlay");
modalOverlay.addEventListener("click", evt => {
  if (evt.target === modalOverlay) {
    cartAdd.style.display = "none";
    modalOverlay.style.display = "none";
  }
});

const featuredOrder = document.querySelector(".featured__order");
featuredOrder.addEventListener("click", evt => {
  evt.preventDefault();

  modalOverlay.style.display = "block";
  cartAdd.style.display = "block";
});

const slides = document.querySelector(".slider__list").children;
let currentSlideIndex = Array.prototype.indexOf.call(
  slides,
  document.querySelector(".slider__slide--current")
);

console.log(currentSlideIndex);

const sliderPrev = document.querySelector(".slider__control--prev");
sliderPrev.addEventListener("click", () => {
  slides[currentSlideIndex--].classList.remove("slider__slide--current");
  if (currentSlideIndex === -1) {
    currentSlideIndex = slides.length - 1;
  }
  slides[currentSlideIndex].classList.add("slider__slide--current");
});

const sliderNext = document.querySelector(".slider__control--next");
sliderNext.addEventListener("click", () => {
  slides[currentSlideIndex++].classList.remove("slider__slide--current");
  if (currentSlideIndex === slides.length) {
    currentSlideIndex = 0;
  }
  slides[currentSlideIndex].classList.add("slider__slide--current");
});
